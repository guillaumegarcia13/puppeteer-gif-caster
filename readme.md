# Welcome to Puppeteer-Gif-Caster

This repository helps you generate GIF files from a website URL and an (_optional_) layer. 

# How To Run

First, open a terminal (`CTRL+ù` in Visual Studio Code).
Then, use the command `npm i` command to install the required NPM packages.
Finally, simply open a terminal and type: 
`node index.js`

## Change configuration

Everything is set in `index.js`.
Feel free to change the `CONFIG` constants to match you needs and play with the parameters so as to obtain the desired quality/size file.