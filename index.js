'use strict';

/**
 * Based on: https://dev.to/aimerib/using-puppeteer-to-make-animated-gifs-of-page-scrolls-1lko 
 */

(async() => {
    const CONFIG = {
        // -- HRC Software
        URL: `https://www.hrc-software.com`,
        GIF_FILENAME: 'HRC_software',
        // -- Agicap
        // URL: `https://agicap.com/fr/`,
        // GIF_FILENAME: 'Agicap',
        // -- WaterAir
        // URL: `https://www.waterair.com/fr`,
        // GIF_FILENAME: 'WaterAir',

        // Layer
        LAYER: 'elon_musk', // filename without the .png extension

        // Parameters
        WIDTH: 1200,
        HEIGHT: 628,
        FRAME_RATE: 60,
        QUALITY: 10,
        DELAY: 500,
        // SCROLL_LENGTH: 100,
        REPEAT: 0,
    };

    const puppeteer = require('puppeteer');
    const GIFEncoder = require('gif-encoder');
    const encoder = new GIFEncoder(CONFIG.WIDTH, CONFIG.HEIGHT);
    const fs = require('fs');
    const getPixels = require('get-pixels');
    const workDir = './temp/';
    const gifDir = './gif/';
    const layerDir = './layer/';

    const mergeImages = require('merge-images');
    const { Canvas, Image } = require('canvas');

    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();

    /**
     *  Preparation
     */

    if (!fs.existsSync(workDir)) {
        fs.mkdirSync(workDir);
    }
    if (!fs.existsSync(gifDir)) {
        fs.mkdirSync(gifDir);
    };

    let file = fs.createWriteStream(gifDir + `${ CONFIG.GIF_FILENAME }.gif`);

    // Setup gif encoder parameters
    encoder.pipe(file);
    encoder.writeHeader();
    encoder.setFrameRate(CONFIG.FRAME_RATE);
    encoder.setQuality(CONFIG.QUALITY);
    encoder.setDelay(CONFIG.DELAY);
    encoder.setRepeat(0);

    // Helper functions declaration
    function addToGif(images, counter = 0) {
        getPixels(images[counter], (err, pixels) => {
            if (err) { console.error('\n** ERROR **\n', err); return; }

            console.log(`Capture ${ counter + 1} / ${ images.length }`);
            encoder.addFrame(pixels.data);
            encoder.read();

            if (counter === images.length - 1) {
                encoder.finish();
                cleanUp(images, function(err) {
                    if (err) {
                        console.log(err);
                    } else {
                        fs.rmdirSync(workDir);
                        console.log('Gif created!');
                        process.exit(0);
                    }
                });
            } else {
                addToGif(images, ++counter);
            }
        });
    };

    function cleanUp(listOfPNGs, callback) {
        let i = listOfPNGs.length;
        listOfPNGs.forEach(function(filepath) {
            fs.unlink(filepath, function(err) {
                i--;
                if (err) {
                    callback(err);
                    return;
                } else if (i <= 0) {
                    callback(null);
                }
            });
        });
    };

    async function closeCookies() {
        await page.evaluate(async() => {
            try {
                // WaterAir
                document.querySelector('#didomi-notice-agree-button').click();
            } catch (e) {}
        });
    }

    async function scrollPage() {
        await page.evaluate(async() => {
            window.scrollBy(0, 100);
        });
    }


    /**
     *  Main logic
     */

    await page.setViewport({ width: CONFIG.WIDTH, height: CONFIG.HEIGHT });
    await page.goto(CONFIG.URL);

    await closeCookies();

    for (let i = 0; i < 60; i++) {
        let screenshotFilename = workDir + `${ i }`;
        await page.screenshot({ path: screenshotFilename + '_.png' });

        await mergeImages([screenshotFilename + '_.png', layerDir + CONFIG.LAYER + '.png'], {
                Canvas: Canvas,
                Image: Image
            })
            .then(b64Merged => {
                // fs.writeFileSync(screenshotFilename + '.png', b64Merged.split('data:image/png;base64,')[1]);
                fs.writeFile(screenshotFilename + '.png', b64Merged.split('data:image/png;base64,')[1], { encoding: 'base64' }, (err) => {
                    console.log(`Wrote File ${ screenshotFilename + '.png' } created`);
                });
            });

        await scrollPage();
        fs.unlinkSync(screenshotFilename + '_.png', (err) => {});
    }

    await browser.close();

    let listOfPNGs = fs.readdirSync(workDir)
        .map(a => a.substr(0, a.length - 4) + '')
        .sort(function(a, b) { return a - b })
        .map(a => workDir + a.substr(0, a.length) + '.png');

    addToGif(listOfPNGs);
})();